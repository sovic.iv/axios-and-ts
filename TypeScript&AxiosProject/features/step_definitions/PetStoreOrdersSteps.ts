const { Given, When, Then } = require('cucumber');
const axios = require('axios');
import { expect } from "chai";
let urlPet = 'https://petstore.swagger.io/v2/pet/';
let urlOrder = 'https://petstore.swagger.io/v2/store/order/';
let status: number;
Given('I have Id {int} of a pet', async function (int: number) {
    // Given('I have Id {float} of a pet', function (float) {
    // Write code here that turns the phrase above into concrete actions
    var message;
    try {
        const response = await axios.get(urlPet + int);
        message = response.data.status;
    } catch (error) {
        console.error(error.message);
    }
    expect(message).to.be.equal("available");
});


When('I try to place an order for purchasing the pet with info', async function (dataTable: any) {
    // Write code here that turns the phrase above into concrete actions
    var informations = dataTable.hashes();
    var params;
    params = {
        "id": informations[0].Id,
        "petId": informations[0].PetId,
        "quantity": informations[0].Quantity,
        "shipDate": informations[0].ShipDate,
        "status": informations[0].Status,
        "complete": informations[0].Complete
    }
    try {
        const response = await axios.post(urlOrder, params);
        status = response.status;
    } catch (error) {
        console.error(error.message);
    }
});


Then('Code should be {int}', function (int: number) {
    // Then('Code should be {float}', function (float) {
    // Write code here that turns the phrase above into concrete actions
    expect(status).to.be.equal(int);
});

When('I try ti delete purchase order by Id {int}', async function (int: number) {
    // When('I try ti delete purchase order by Id {float}', function (float) {
    // Write code here that turns the phrase above into concrete actions
    try {
        const response = await axios.delete(urlOrder + int);
        status = response.status;
    } catch (error) {
        console.error(error.message);
    }
});