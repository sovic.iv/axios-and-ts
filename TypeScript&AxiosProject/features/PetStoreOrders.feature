Feature: PetStore Orders

    Scenario Outline: PetStore Orders
        Given I have Id <PetId> of a pet
        When I try to place an order for purchasing the pet with info
            | Id   | PetId   | Quantity   | ShipDate   | Status   | Complete   |
            | <Id> | <PetId> | <Quantity> | <ShipDate> | <Status> | <Complete> |
        Then Code should be 200
        When I try ti delete purchase order by Id <PurchaseOrderId>
        Then Code should be 200
        Examples:
            | Id | PetId               | Quantity | ShipDate                 | Status | Complete | PurchaseOrderId |
            | 0  | 9222968140496991000 | 1        | 2021-01-18T13:13:14.787Z | placed | true     | 21              |